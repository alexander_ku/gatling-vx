
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.ObjectMapper
import io.gatling.core.Predef._
import io.gatling.core.json.Jackson
import io.gatling.http.Predef._
import com.tinypass.client._

import scala.concurrent.duration._


class OfferMode2Simulation extends Simulation {

  var csvFeeder = csv("offerid_aid_mode2.csv")

  val httpProtocol = http
    .baseURL("http://localtest.me:8080")
    //    .baseURL("http://192.168.1.169:8080")
    .inferHtmlResources(BlackList(""".*\.js""", """.*\.css.*""", """.*\.gif""", """.*\.jpeg""", """.*\.jpg""", """.*\.ico""", """.*\.woff""", """.*\.(t|o)tf""", """.*\.png"""), WhiteList())
    .acceptHeader("*/*")
    .acceptEncodingHeader("gzip, deflate, sdch")
    .acceptLanguageHeader("en-US,en;q=0.8")
    .doNotTrackHeader("1")
    .userAgentHeader("Chrome/53.0.2785.116 Safari/537.36")

  val headers_0 = Map(
    "Accept" -> "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
    "Upgrade-Insecure-Requests" -> "1")

  val headers_ajax = Map(
    "Accept" -> "application/json, text/plain, */*",
    "Accept-Encoding" -> "gzip, deflate",
    "Content-Type" -> "application/json;charset=UTF-8",
    "Ng-Request" -> "1",
    "Origin" -> "http://localtest.me:8080",
    "X-Requested-With" -> "XMLHttpRequest")

  val headers_authenticated = Map(
    "userToken" -> "${user_token}"
  ) ++ headers_ajax


  def logFail(fail: String) {
    println(s"shit happend: $fail")
  }

  //TODO: san
  var user_token =
  """{jox}{"uid":"${UID}","email":"${EMAIL}","timestamp":1234567890}""".stripMargin

  val scn = scenario("OfferMode2Simulation").feed(csvFeeder).repeat(5) {
    exec(http("offerShow ${OFFER_ID}")
      .get("/checkout/offer/show?aid=${AID}&offerId=${OFFER_ID}&userToken=" + user_token + "&displayMode=modal&" +
        "url=http%3A%2F%2Flocaltest.me%3A8080%2Fcheckout%2Ftest.jsp&debug=false" +
        "&userProvider=tinypass_accounts&hasLoginRequiredCallback=false")
      .headers(headers_0)
      .check(regex("\"error\":\"(.*)\"").count.is(0).saveAs("TPParam"))
    )

//        .exec { session =>
//      val TPParam: String = session.get("TPParam").as[String]
//      val error_pos: Int = TPParam.indexOf("\"error\":")
//      if (error_pos > -1) {
//        println("Res1=> " + TPParam.substring(error_pos + 8, 64 + error_pos))
//        session.markAsFailed
//      }
//      session
//    }

  }
  //      .resources(http("loadTranslationMap")
  //        .get("/checkout/general/loadTranslationMap?aid=${AID}&version="+System.currentTimeMillis()+"&language=en_US"),
  //        http("platformTranslationMap")
  //          .get("/ng/common/i18n/platform-translation-map_en_US.js?version=1475230030688")))


  setUp(scn.inject(rampUsers(1) over (2 seconds))).protocols(httpProtocol)

}