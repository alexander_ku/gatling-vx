
import io.gatling.core.Predef._
import io.gatling.http.Predef._

class AdminConsoleSimulation /*extends Simulation */{

	val httpProtocol = http
		.baseURL("http://localtest.me:8080")
		.inferHtmlResources(BlackList(""".*\.js.*""", """.*\.css.*""", """.*\.gif""", """.*\.jpeg""", """.*\.jpg""", """.*\.ico""", """.*\.woff""", """.*\.(t|o)tf""", """.*\.png"""), WhiteList())
		.acceptHeader("application/json, text/plain, */*")
		.acceptEncodingHeader("gzip, deflate, sdch")
		.acceptLanguageHeader("en-US,en;q=0.8")
		.doNotTrackHeader("1")
		.userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36")

	val headers_0 = Map(
		"Accept" -> "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
		"Upgrade-Insecure-Requests" -> "1")

	val headers_1 = Map("Accept" -> "image/webp,image/*,*/*;q=0.8")

	val headers_2 = Map(
		"Accept" -> "*/*",
		"Accept-Encoding" -> "gzip, deflate",
		"Origin" -> "http://localtest.me:8080",
		"X-Requested-With" -> "XMLHttpRequest")

	val headers_5 = Map(
		"Ng-Request" -> "1",
		"X-Requested-With" -> "XMLHttpRequest")

	val scn = scenario("admin dashboard")
		.exec(http("login")
			.post("/login/login?_ajax=true&_stamp=1474897549066")
			.headers(headers_2)
			.formParam("orig", "")
			.formParam("email", "tim@tinypass.com")
			.formParam("password", "p")
			.formParam("captchaLoginHandler", "")
			.formParam("twofactorCode", "")
			  .check(headerRegex("execute_js_after", "setMessage\\('error'").notExists)//TODO:
			.resources(http("admin home")
			.get("/admin")
			.headers(headers_0),
           http("loadApplications")
			.get("/admin/applications/loadApplications?_tpn=yytGBAgJxv&query=&state=-1")
			.headers(headers_5)))
		.exec(http("loadApplications with filter")
			.get("/admin/applications/loadApplications?_tpn=yytGBAgJxv&mode=&offset=0&query=modetw&state=-1")
			.headers(headers_5))
		.exec(http("editApp")
			.get("/admin/applications/editApp?aid=MODETWOAPP")
			.headers(headers_0)
			.resources(http("videoAuthList")
			.get("/admin/applications/videoAuthList?aid=MODETWOAPP")
			.headers(headers_5),
            http("deliveryZoneList")
			.get("/admin/applications/deliveryZoneList?aid=MODETWOAPP")
			.headers(headers_5),
            http("userProviderList")
			.get("/admin/applications/userProviderList?aid=MODETWOAPP")
			.headers(headers_5),
            http("configurationsList")
			.get("/admin/applications/configurationsList?aid=MODETWOAPP")
			.headers(headers_5),
            http("loadApp")
			.get("/admin/applications/loadApp?aid=MODETWOAPP")
			.headers(headers_5),
            http("loadSalesTaxes")
			.get("/admin/tax/loadSalesTaxes?aid=MODETWOAPP")
			.headers(headers_5)))

//		setUp(scn.inject(atOnceUsers(3))).protocols(httpProtocol)
}