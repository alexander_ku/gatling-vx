
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._



class OfferMode4Simulation /*extends Simulation */{

  var csvFeeder = csv("offerid_aid_mode4.csv")

  val httpProtocol = http
    .baseURL("http://localtest.me:8080")
    //    .inferHtmlResources(BlackList(""".*\.js""", """.*\.css.*""", """.*\.gif""", """.*\.jpeg""", """.*\.jpg""", """.*\.ico""", """.*\.woff""", """.*\.(t|o)tf""", """.*\.png"""), WhiteList())
    .acceptHeader("*/*")
    .acceptEncodingHeader("gzip, deflate, sdch")
    .acceptLanguageHeader("en-US,en;q=0.8")
    .doNotTrackHeader("1")
    .userAgentHeader("Chrome/53.0.2785.116 Safari/537.36")

  val headers_0 = Map(
    "Accept" -> "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
    "Upgrade-Insecure-Requests" -> "1")

  val headers_ajax = Map(
    "Accept" -> "application/json, text/plain, */*",
    "Accept-Encoding" -> "gzip, deflate",
    "Content-Type" -> "application/json;charset=UTF-8",
    "Ng-Request" -> "1",
    "Origin" -> "http://localtest.me:8080",
    "X-Requested-With" -> "XMLHttpRequest")

  val headers_authenticated = Map(
    "userToken" -> "${user_token}"
  ) ++ headers_ajax


  def logFail(fail: String) {
    println(s"shit happend: $fail")
  }


  val loginRequestTemplate =
    """
{
"aid":"${AID}",
"email":"${EMAIL}",
"password": "p"
}
    """

  val scn = scenario("OfferMode4Simulation").feed(csvFeeder)
    .exec(http("loginShow")
      .get("/checkout/user/loginShow?displayMode=popup&width=360&height=660&state=login" +
        "&iframeId=HglX1fer0XBKKdEu&url=http%3A%2F%2Flocaltest.me%3A8080%2Fcheckout%2Ftest.jsp%3Fmode%3D4%23" +
        "&aid=${AID}&user_provider=tinypass_accounts")
      .headers(headers_0))
    .exec(http("userLogin")
      .post("/checkout/user/login")
      .headers(headers_ajax)
      .body(StringBody(loginRequestTemplate)).check(jsonPath("$.models.user_token").exists.saveAs("user_token")))
    .exec(http("offerShow")
      .get("/checkout/offer/show?aid=${AID}&offerId=${OFFER_ID}&userToken=${user_token}&displayMode=modal&" +
        "url=http%3A%2F%2Flocaltest.me%3A8080%2Fcheckout%2Ftest.jsp&debug=false" +
        "&userProvider=tinypass_accounts&hasLoginRequiredCallback=false")
      .headers(headers_authenticated)
      .resources(http("loadTranslationMap")
        .get("/checkout/general/loadTranslationMap?aid=${AID}&version="+System.currentTimeMillis()+"&language=en_US"),
        http("platformTranslationMap")
          .get("/ng/common/i18n/platform-translation-map_en_US.js?version=1475230030688")))

//

//  setUp(scn.inject(rampUsers(100) over(2 minutes))).protocols(httpProtocol)

}